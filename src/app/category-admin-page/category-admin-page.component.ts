import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Category} from '../category';
import {ApiService} from '../api.service';
import {faEdit, faPlus, faTimes} from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClipsFormComponent } from '../clips-form/clips-form.component';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-category-admin-page',
  templateUrl: './category-admin-page.component.html',
  styleUrls: ['./category-admin-page.component.scss']
})
export class CategoryAdminPageComponent implements OnInit {

  category: Category;
  showSpinner: boolean;
  deleteIcon = faTimes;
  editIcon = faEdit;
  addIcon = faPlus;

  constructor(private apiService: ApiService, private route: ActivatedRoute, private modalService: NgbModal, public toastService: ToastService) { }

  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      const catId = params.get('id');
      this.getCategory(catId);
    });

  }

  getCategory(id) {
    this.showSpinner = true;

    this.apiService.getCategoryById(id).subscribe((data) => {
      this.category = data;

      this.showSpinner = false;
    });
  }

  openNewClip() {
    const modal = this.modalService.open(ClipsFormComponent, {
      centered: true,
      beforeDismiss: () => {
        this.getCategory(this.category.id);
        return true;
      }
    });
    modal.componentInstance.categoryId = this.category.id;
  }

  openEditClip(id: number) {
    const modal = this.modalService.open(ClipsFormComponent, {
      centered: true,
      beforeDismiss: () => {
        this.getCategory(this.category.id);
        return true;
      }
    });
    modal.componentInstance.new = false;
    modal.componentInstance.clipId = id;
  }

  deleteClip(id: number) {
    this.showSpinner = true;

    this.apiService.deleteClipById(id).subscribe((data) => {
      this.showSpinner = false;
      this.showSuccess('Clip Deleted');
      this.getCategory(this.category.id);
    });
  }

  showSuccess(text: string) {
    this.toastService.show('Success', text, {
      classname: 'bg-success text-light'
    });
  }

}
