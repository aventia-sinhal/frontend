import {Component, Input, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../api.service';
import { Category } from '../category';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit {

  category: Category = {
    id: 0,
    name: '',
    active: true,
    clips: null
  };

  categoryForm: FormGroup;

  showSpinner;

  @Input() new = true;
  @Input() categoryId = 0;

  constructor(private apiService: ApiService, public activeModal: NgbActiveModal, public toastService: ToastService) { }

  submitForm() {
    this.showSpinner = true;

    const formData = this.categoryForm.value;

    const category: Category = {
      id: formData.CategoryId,
      name: formData.Name,
      active: formData.Active,
      clips: null
    };

    if (!this.new) {
      this.apiService.updateCategory(category, category.id).subscribe((data) => {
        this.apiService.getCategoryById(this.categoryId).subscribe((data1) => {
          this.category = data1;
          this.showSpinner = false;
          this.activeModal.dismiss();
          this.showSuccess('Category Updated');
        });
        this.showSpinner = false;
      });
    } else {
      this.apiService.newCategory(category).subscribe((data) => {
        this.showSpinner = false;
        this.activeModal.dismiss();
        this.showSuccess('New Category Added');
      });
    }
  }

  ngOnInit() {

    this.categoryForm = new FormGroup({
      Name: new FormControl('', Validators.required),
      Active: new FormControl(''),
      CategoryId: new FormControl('')
    });

    if (!this.new) {
      this.showSpinner = true;

      this.apiService.getCategoryById(this.categoryId).subscribe((data) => {
        this.category = data;
        this.showSpinner = false;

        this.categoryForm.setValue({
          Name: this.category.name,
          Active: this.category.active,
          CategoryId: this.category.id
        });

      });
    }
  }

  showSuccess(text: string) {
    this.toastService.show('Success', text, {
      classname: 'bg-success text-light'
    });
  }
}
