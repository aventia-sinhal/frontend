import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RoutingModule } from './routing/routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgBootstrapFormValidationModule} from 'ng-bootstrap-form-validation';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RouterModule } from '@angular/router';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { FooterComponent } from './footer/footer.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { CategoryFormComponent } from './category-form/category-form.component';
import { CategoryAdminPageComponent } from './category-admin-page/category-admin-page.component';
import { ClipsFormComponent } from './clips-form/clips-form.component';
import { ToastContainerComponent } from './toast-container/toast-container.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomePageComponent,
    AdminPageComponent,
    FooterComponent,
    SpinnerComponent,
    CategoryFormComponent,
    CategoryAdminPageComponent,
    ClipsFormComponent,
    ToastContainerComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    RouterModule,
    RoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgBootstrapFormValidationModule.forRoot()
  ],
  entryComponents: [
    CategoryFormComponent,
    ClipsFormComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
