import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule, Route} from '@angular/router';
import {HomePageComponent} from '../home-page/home-page.component';
import {AdminPageComponent} from '../admin-page/admin-page.component';
import {CategoryAdminPageComponent} from '../category-admin-page/category-admin-page.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomePageComponent},
  {path: 'admin', component: AdminPageComponent},
  {path: 'admin/category/:id', component: CategoryAdminPageComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule { }
