import {Injectable, TemplateRef} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  toasts: any[] = [];

  show(header: string, body: string, options: {}) {
    this.toasts.push({ header, body, options });
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }

  constructor() { }
}
