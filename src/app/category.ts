import {Clip} from './clip.mediaClip';

export interface Category {

  id: number;
  name: string;
  active: boolean;
  clips: Clip[];

}
