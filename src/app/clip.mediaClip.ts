export interface Clip {

  id: number;
  name: string;
  active: boolean;
  categoryId: string;

}
