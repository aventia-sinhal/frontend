import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from './category';
import {Clip} from './clip.mediaClip';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  BASE_URL = 'aventiabackend.ihemsedal.no';
  BASE_PATH = '/api';
  BASE_PORT = '443';

  constructor(private httpClient: HttpClient) {
    if (!environment.production) {
      this.BASE_URL = 'localhost';
      this.BASE_PORT = '5001';
    }
  }

  public getAllCategories() {
    return this.httpClient.get<Category[]>('https://' + this.BASE_URL + ':' + this.BASE_PORT + this.BASE_PATH + '/category');
  }

  public getCategoryById(id: number) {
    return this.httpClient.get<Category>('https://' + this.BASE_URL + ':' + this.BASE_PORT + this.BASE_PATH + '/category/' + id);
  }

  public updateCategory(category: Category, id: number) {

    const formData = new FormData();

    formData.append('name', category.name);

    let categoryActive = '0';
    if (category.active) {
      categoryActive = '1';
    }

    formData.append('active', categoryActive);

    return this.httpClient.put('https://' + this.BASE_URL + ':' + this.BASE_PORT + this.BASE_PATH + '/category/' + id, formData);
  }

  public newCategory(category: Category) {

    const formData = new FormData();

    formData.append('name', category.name);

    let categoryActive = '0';
    if (category.active) {
      categoryActive = '1';
    }

    formData.append('active', categoryActive);

    return this.httpClient.post('https://' + this.BASE_URL + ':' + this.BASE_PORT + this.BASE_PATH + '/category', formData);
  }

  public deleteCategoryById(id: number) {
    return this.httpClient.delete<Category>('https://' + this.BASE_URL + ':' + this.BASE_PORT + this.BASE_PATH + '/category/' + id);
  }

  public getClipById(id: number) {
    return this.httpClient.get<Clip>('https://' + this.BASE_URL + ':' + this.BASE_PORT + this.BASE_PATH + '/clips/' + id);
  }

  public deleteClipById(id: number) {
    return this.httpClient.delete<Category>('https://' + this.BASE_URL + ':' + this.BASE_PORT + this.BASE_PATH + '/clips/' + id);
  }

  public newClip(clip: Clip) {

    const formData = new FormData();

    formData.append('name', clip.name);
    formData.append('cat_id', clip.categoryId);

    let clipActive = '0';
    if (clip.active) {
      clipActive = '1';
    }

    formData.append('active', clipActive);

    return this.httpClient.post('https://' + this.BASE_URL + ':' + this.BASE_PORT + this.BASE_PATH + '/clips', formData);
  }

  public updateClip(clip: Clip) {

    const formData = new FormData();

    formData.append('name', clip.name);

    let clipActive = '0';
    if (clip.active) {
      clipActive = '1';
    }

    formData.append('active', clipActive);

    return this.httpClient.put('https://' + this.BASE_URL + ':' + this.BASE_PORT + this.BASE_PATH + '/clips/' + clip.id, formData);
  }

}
