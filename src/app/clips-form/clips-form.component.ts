import { Component, Input, OnInit } from '@angular/core';
import { Clip} from '../clip.mediaClip';
import { ApiService } from '../api.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-clips-form',
  templateUrl: './clips-form.component.html',
  styleUrls: ['./clips-form.component.scss']
})
export class ClipsFormComponent implements OnInit {

  clip: Clip = {
    id: 0,
    name: '',
    active: true,
    categoryId: ''
  };

  clipsForm: FormGroup;

  showSpinner;

  @Input() new = true;
  @Input() categoryId = 0;
  @Input() clipId = 0;

  constructor(private apiService: ApiService, public activeModal: NgbActiveModal, public toastService: ToastService) { }

  submitForm() {
    this.showSpinner = true;

    const formData = this.clipsForm.value;

    const clip: Clip = {
      id: formData.ClipId,
      name: formData.Name,
      active: formData.Active,
      categoryId: formData.CategoryId
    };

    if (!this.new) {
      this.apiService.updateClip(clip).subscribe((data) => {
        this.showSpinner = false;
        this.activeModal.dismiss();
        this.showSuccess('Clip Updated');
      });
    } else {
      this.apiService.newClip(clip).subscribe((data) => {
        this.showSpinner = false;
        this.activeModal.dismiss();
        this.showSuccess('New Clip Added');
      });
    }
  }

  ngOnInit() {

    this.clipsForm = new FormGroup({
      Name: new FormControl('', Validators.required),
      Active: new FormControl(''),
      ClipId: new FormControl(''),
      CategoryId: new FormControl('')
    });

    if (!this.new) {
      this.showSpinner = true;

      this.apiService.getClipById(this.clipId).subscribe((data) => {
        this.clip = data;
        this.updateForm();
        this.showSpinner = false;
      });
    } else {
      this.clipsForm.get('CategoryId')
        .setValue(this.categoryId);
    }
  }

  updateForm() {
    this.clipsForm.setValue({
      Name: this.clip.name,
      Active: this.clip.active,
      ClipId: this.clip.id,
      CategoryId: this.clip.categoryId
    });
  }

  showSuccess(text: string) {
    this.toastService.show('Success', text, {
      classname: 'bg-success text-light'
    });
  }
}
