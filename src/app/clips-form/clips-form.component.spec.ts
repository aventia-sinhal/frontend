import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClipsFormComponent } from './clips-form.component';

describe('ClipsFormComponent', () => {
  let component: ClipsFormComponent;
  let fixture: ComponentFixture<ClipsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClipsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClipsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
