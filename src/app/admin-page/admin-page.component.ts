import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Category } from '../category';
import { faTimes, faEdit, faPlus } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryFormComponent } from '../category-form/category-form.component';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit {

  categories: Category[];
  showSpinner;
  deleteIcon = faTimes;
  editIcon = faEdit;
  addIcon = faPlus;

  constructor(private apiService: ApiService, private modalService: NgbModal, public toastService: ToastService) { }

  openNewCategory() {
    const modal = this.modalService.open(CategoryFormComponent, {
      centered: true,
      beforeDismiss: () => {
        this.loadCategories();
        return true;
      }
    });
  }

  openEditCategory(id: number) {
    const modal = this.modalService.open(CategoryFormComponent, {
      centered: true,
      beforeDismiss: () => {
        this.loadCategories();
        return true;
      }
    });
    modal.componentInstance.new = false;
    modal.componentInstance.categoryId = id;
  }

  deleteCategory(id: number) {
    this.showSpinner = true;

    this.apiService.deleteCategoryById(id).subscribe((data) => {
      this.showSpinner = false;
      this.showSuccess('Category Deleted');
      this.loadCategories();
    });
  }

  loadCategories() {
    this.showSpinner = true;

    this.apiService.getAllCategories().subscribe((data) => {
      this.categories = data;

      this.showSpinner = false;
    });
  }

  showSuccess(text: string) {
    this.toastService.show('Success', text, {
      classname: 'bg-success text-light'
    });
  }

  ngOnInit() {

    this.loadCategories();

  }

}
