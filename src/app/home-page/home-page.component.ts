import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Category} from '../category';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  categories: Category[];
  showSpinner;

  constructor(private apiService: ApiService) { }

  ngOnInit() {

    this.showSpinner = true;

    this.apiService.getAllCategories().subscribe((data) => {


      data.forEach(obj => {
        let clips = [];

        obj.clips.forEach(clip => {
          if (clip.active) {
            clips.push(clip);
          }
        });
        obj.clips = clips;
      });

      this.categories = data;

      this.showSpinner = false;
    });

  }
}
